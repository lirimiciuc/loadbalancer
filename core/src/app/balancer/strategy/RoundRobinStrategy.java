package balancer.strategy;

import balancer.provider.IProvider;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntBinaryOperator;

public class RoundRobinStrategy implements IBalanceStrategy {

    private final AtomicInteger roundRobinProviderIndex = new AtomicInteger(-1);

    @Override
    public IProvider nextProvider(List<IProvider> availableProviders) {
        IntBinaryOperator accumulator = (oldIndex, inc) -> (oldIndex + 1 >= availableProviders.size() ? 0 : oldIndex + 1);
        int providerIndex = roundRobinProviderIndex.accumulateAndGet(1, accumulator);
        return availableProviders.get(providerIndex);
    }

    AtomicInteger getRoundRobinProviderIndex() {
        return roundRobinProviderIndex;
    }
}
