package balancer.strategy;

import balancer.provider.IProvider;

import java.util.List;

public interface IBalanceStrategy {
    IProvider nextProvider(List<IProvider> availableProviders);
}
