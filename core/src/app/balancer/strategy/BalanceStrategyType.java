package balancer.strategy;

public enum BalanceStrategyType {
    RANDOM, ROUND_ROBIN
}
