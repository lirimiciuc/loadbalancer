package balancer.strategy;

public class BalanceStrategyFactory {

    public static IBalanceStrategy create(BalanceStrategyType type) {
        return switch (type) {
            case RANDOM -> new RandomStrategy();
            case ROUND_ROBIN -> new RoundRobinStrategy();
        };
    }
}
