package balancer.strategy;

import balancer.provider.IProvider;

import java.util.List;
import java.util.Random;

public class RandomStrategy implements IBalanceStrategy {
    private final Random random = new Random();

    @Override
    public IProvider nextProvider(List<IProvider> availableProviders) {
        return availableProviders.get(random.nextInt(availableProviders.size()));
    }
}
