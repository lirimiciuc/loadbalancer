package balancer;

import balancer.provider.IProvider;
import balancer.strategy.IBalanceStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.SECONDS;

public class LoadBalancer {

    private static final int maxProviders = 10;
    private final List<IProvider> providers = new ArrayList<>();
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);
    private final AtomicInteger openConnections = new AtomicInteger(0);
    /**
     * Making this thread safe, should guarantee thread safety for get/include/exclude available providers.
     * TODO: even better would be be keep the active state on the provider itself. maybe even the remaining attempts
     * TODO: as a consequence, we should be able to remove excludedProvidersWithSuccessfulAttempts
     */
    private final ConcurrentMap<String, Integer> excludedProvidersWithSuccessfulAttempts = new ConcurrentHashMap<>();
    private final Runnable alivenessProbe = () -> providers.forEach(provider -> {
        if (provider.check()) {
            if (excludedProvidersWithSuccessfulAttempts.containsKey(provider.getId())) {
                Integer successfulAttempts = excludedProvidersWithSuccessfulAttempts.get(provider.getId());
                if (successfulAttempts == 1) {
                    includeProvider(provider.getId());
                } else {
                    excludedProvidersWithSuccessfulAttempts.put(provider.getId(), 1);
                }
            }
        } else {
            excludeProvider(provider.getId());
        }
    });
    private IBalanceStrategy balanceStrategy;
    private Integer providerParallelism = 2;
    private Integer healthCheckInterval = 30;

    public void startCheckingProvidersHealth() {
        scheduler.scheduleAtFixedRate(alivenessProbe, healthCheckInterval, healthCheckInterval, SECONDS);
    }

    /**
     * The assumption here is that we allow Y*noOfProviders connections pe Balancer instance,
     * and not Y connection per provider (as per my understanding of the requirement)
     */
    public CompletableFuture<String> get() {
        if (providers.isEmpty()) {
            return CompletableFuture.failedFuture(new IllegalStateException("Invalid load balancer state"));
        }

        if (openConnections.get() >= getAvailableProviders().size() * providerParallelism) {
            return CompletableFuture.failedFuture(new UnsupportedOperationException("Max open connections reached"));
        } else {
            openConnections.incrementAndGet();
        }

        IProvider provider = balanceStrategy.nextProvider(getAvailableProviders());

        return provider.get().whenComplete((s, throwable) -> openConnections.decrementAndGet());
    }

    private List<IProvider> getAvailableProviders() {
        return providers.stream()
                .filter(provider -> !excludedProvidersWithSuccessfulAttempts.containsKey(provider.getId()))
                .collect(Collectors.toList());
    }

    public void excludeProvider(String providerId) {
        excludedProvidersWithSuccessfulAttempts.put(providerId, 0);
    }

    public void includeProvider(String providerId) {
        excludedProvidersWithSuccessfulAttempts.remove(providerId);
    }

    public Set<String> getExcludedProvidersWithSuccessfulAttempts() {
        return excludedProvidersWithSuccessfulAttempts.keySet();
    }

    /**
     * The assumption here is that we don't allow at all more than maxProvides.
     * Another approach would be to return true/false if we can successfully add a new provider or not.
     */
    public void registerProviders(List<IProvider> newProviders) {
        if (newProviders.size() + providers.size() <= maxProviders) {
            providers.addAll(newProviders);
        } else {
            throw new IllegalArgumentException(String.format("Up to %s providers are supported ", maxProviders));
        }
    }

    public LoadBalancer setHealthCheckInterval(Integer healthCheckInterval) {
        this.healthCheckInterval = healthCheckInterval;
        return this;
    }

    public List<IProvider> getProviders() {
        return providers;
    }

    public LoadBalancer setBalanceStrategy(IBalanceStrategy balanceStrategy) {
        this.balanceStrategy = balanceStrategy;
        return this;
    }

    public AtomicInteger getOpenConnections() {
        return openConnections;
    }

    public LoadBalancer setProviderParallelism(Integer providerParallelism) {
        this.providerParallelism = providerParallelism;
        return this;
    }
}
