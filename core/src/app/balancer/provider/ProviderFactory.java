package balancer.provider;

public class ProviderFactory {
    public static IProvider create() {
        return new SimpleProvider();
    }
}
