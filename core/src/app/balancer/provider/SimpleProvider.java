package balancer.provider;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class SimpleProvider implements IProvider {

    private final String uuid = UUID.randomUUID().toString();

    public CompletableFuture<String> get() {
        return CompletableFuture.supplyAsync(() -> uuid);
    }

    @Override
    public boolean check() {
        return new Random().nextBoolean();
    }

    @Override
    public String getId() {
        return uuid;
    }
}
