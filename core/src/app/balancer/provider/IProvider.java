package balancer.provider;

import java.util.concurrent.CompletableFuture;

public interface IProvider {
    CompletableFuture<String> get();

    boolean check();

    String getId();
}
