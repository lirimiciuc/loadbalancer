package balancer;

import balancer.provider.IProvider;
import balancer.provider.ProviderFactory;
import balancer.strategy.BalanceStrategyFactory;
import balancer.strategy.BalanceStrategyType;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        System.out.println("Initializing Load balancer with round robin strategy..");

        LoadBalancer loadBalancer = new LoadBalancer()
                .setProviderParallelism(3)
                .setBalanceStrategy(BalanceStrategyFactory.create(BalanceStrategyType.ROUND_ROBIN));
        loadBalancer.startCheckingProvidersHealth();

        IProvider provider = ProviderFactory.create();
        loadBalancer.registerProviders(List.of(ProviderFactory.create(), provider, ProviderFactory.create()));
        System.out.println("Requesting...");
        for (int i = 1; i <= 9; i++) {
            loadBalancer.get().whenComplete((s, throwable) -> {
                System.out.println("response:" + s);
            });
        }

        System.out.printf("Deactivating provider %s...%n", provider.getId());
        loadBalancer.excludeProvider(provider.getId());
        System.out.println("Requesting again...");
        for (int i = 1; i <= 6; i++) {
            loadBalancer.get().whenComplete((s, throwable) -> {
                System.out.println("response:" + s);
            });
        }

    }
}
