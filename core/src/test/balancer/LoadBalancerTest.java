package balancer;

import balancer.provider.IProvider;
import balancer.provider.SimpleProvider;
import balancer.strategy.RandomStrategy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class LoadBalancerTest {

    @Test
    void allowAddingProviders_whenLimitNotExceeded() {
        LoadBalancer loadBalancer = new LoadBalancer();
        loadBalancer.registerProviders(List.of(new SimpleProvider(), new SimpleProvider()));
        loadBalancer.registerProviders(List.of(new SimpleProvider(), new SimpleProvider(), new SimpleProvider(), new SimpleProvider()));

        Assertions.assertEquals(6, loadBalancer.getProviders().size());
    }

    @Test
    void throwsException_whenLimitNotExceeded() {
        LoadBalancer loadBalancer = new LoadBalancer();

        List<IProvider> tooMany = providersList(11);
        Assertions.assertThrows(IllegalArgumentException.class, () -> loadBalancer.registerProviders(tooMany));
    }

    private List<IProvider> providersList(int count) {
        return IntStream.range(0, count).mapToObj(val -> new SimpleProvider()).collect(Collectors.toList());
    }

    @Test
    void excludeProviders_whenAlivenessFails() throws InterruptedException {
        LoadBalancer loadBalancer = new LoadBalancer();
        loadBalancer.setHealthCheckInterval(1);
        loadBalancer.registerProviders(List.of(
                new DummyProvider(DummyIds.ID1).setAlive(true),
                new DummyProvider(DummyIds.ID2).setAlive(false),
                new DummyProvider(DummyIds.ID3).setAlive(true)));

        loadBalancer.startCheckingProvidersHealth();
        Thread.sleep(2 * 1000);

        Assertions.assertEquals(Set.of(DummyIds.ID2), loadBalancer.getExcludedProvidersWithSuccessfulAttempts());
    }

    @Test
    void reIncludeProvider_afterTwoConsecutiveSuccessfulAttempts() throws InterruptedException {
        LoadBalancer loadBalancer = new LoadBalancer();
        int checkInvocationDelaySeconds = 2;
        loadBalancer.setHealthCheckInterval(checkInvocationDelaySeconds);
        DummyProvider dummyProvider = new DummyProvider(DummyIds.ID2).setAlive(false);
        loadBalancer.registerProviders(List.of(dummyProvider));

        loadBalancer.startCheckingProvidersHealth();
        Thread.sleep(checkInvocationDelaySeconds * 1100);
        Assertions.assertArrayEquals(Set.of(DummyIds.ID2).toArray(), loadBalancer.getExcludedProvidersWithSuccessfulAttempts().toArray());

        dummyProvider.setAlive(true);
        Thread.sleep(3 * checkInvocationDelaySeconds * 1100); // we wait at least 2 invocation cycles

        Assertions.assertEquals(Set.of(), loadBalancer.getExcludedProvidersWithSuccessfulAttempts());
    }

    @Test
    void doNotIncludeProvider_whenNoTwoConsecutiveSuccessfulAttempts() throws InterruptedException {
        int checkInvocationDelaySeconds = 2;
        DummyProvider dummyProvider = new DummyProvider(DummyIds.ID2).setAlive(false);
        LoadBalancer loadBalancer = new LoadBalancer().setHealthCheckInterval(checkInvocationDelaySeconds);
        loadBalancer.registerProviders(List.of(dummyProvider));

        loadBalancer.startCheckingProvidersHealth();
        Thread.sleep(checkInvocationDelaySeconds * 1100);
        Assertions.assertArrayEquals(Set.of(DummyIds.ID2).toArray(), loadBalancer.getExcludedProvidersWithSuccessfulAttempts().toArray());

        dummyProvider.setAlive(true);
        Thread.sleep(checkInvocationDelaySeconds * 1200);
        Assertions.assertArrayEquals(Set.of(DummyIds.ID2).toArray(), loadBalancer.getExcludedProvidersWithSuccessfulAttempts().toArray());

        dummyProvider.setAlive(false);
        Thread.sleep(checkInvocationDelaySeconds * 1200);
        Assertions.assertArrayEquals(Set.of(DummyIds.ID2).toArray(), loadBalancer.getExcludedProvidersWithSuccessfulAttempts().toArray());
    }

    @Test
    void allowNewRequest_whileMaxConnectionsNotReached() throws InterruptedException {

        LoadBalancer loadBalancer = new LoadBalancer().setProviderParallelism(2).setBalanceStrategy(new RandomStrategy());
        loadBalancer.registerProviders(List.of(new LogRunningProvider(DummyIds.ID1), new LogRunningProvider(DummyIds.ID2)));

        for (int i = 1; i <= 4; i++) {
            loadBalancer.get();
        }
        Thread.sleep(4 * 1000);
        Assertions.assertEquals(new AtomicInteger(0).get(), loadBalancer.getOpenConnections().get());
    }

    @Test
    void notAllowNewRequest_whenMaxConnectionsReached() throws InterruptedException {

        LoadBalancer loadBalancer = new LoadBalancer().setProviderParallelism(2).setBalanceStrategy(new RandomStrategy());
        loadBalancer.registerProviders(List.of(new LogRunningProvider(DummyIds.ID1), new LogRunningProvider(DummyIds.ID2)));

        for (int i = 1; i <= 4; i++) {
            loadBalancer.get();
        }
        Assertions.assertThrows(CompletionException.class, () -> loadBalancer.get().join());

        Thread.sleep(4000);
        Assertions.assertEquals(new AtomicInteger(0).get(), loadBalancer.getOpenConnections().get());
    }

    interface DummyIds {
        String ID1 = UUID.randomUUID().toString();
        String ID2 = UUID.randomUUID().toString();
        String ID3 = UUID.randomUUID().toString();
    }

    static class DummyProvider implements IProvider {

        private final String uuid;
        private Boolean alive;

        public DummyProvider(String uuid) {
            this.uuid = uuid;
        }

        public CompletableFuture<String> get() {
            return CompletableFuture.completedFuture(uuid);
        }

        @Override
        public boolean check() {
            return alive;
        }

        @Override
        public String getId() {
            return uuid;
        }

        public DummyProvider setAlive(Boolean alive) {
            this.alive = alive;
            return this;
        }
    }

    static class LogRunningProvider extends DummyProvider {

        public LogRunningProvider(String uuid) {
            super(uuid);
        }

        public CompletableFuture<String> get() {
            return CompletableFuture.supplyAsync(() -> {
                // Simulate a long-running Job
                try {
                    TimeUnit.SECONDS.sleep(3);

                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }

                return super.uuid;
            });
        }
    }
}
