package balancer.strategy;

import balancer.provider.IProvider;
import balancer.LoadBalancer;
import balancer.provider.SimpleProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.ExecutionException;

class RoundRobinStrategyTest {

    @Test
    void roundRobin() {
        LoadBalancer loadBalancer = new LoadBalancer();
        RoundRobinStrategy balanceStrategy = new RoundRobinStrategy();
        loadBalancer.setBalanceStrategy(balanceStrategy);
        loadBalancer.registerProviders(List.of(new SimpleProvider(), new SimpleProvider(), new SimpleProvider()));

        loadBalancer.get();
        Assertions.assertEquals(0, balanceStrategy.getRoundRobinProviderIndex().get());
        loadBalancer.get();
        Assertions.assertEquals(1, balanceStrategy.getRoundRobinProviderIndex().get());
        loadBalancer.get();
        Assertions.assertEquals(2, balanceStrategy.getRoundRobinProviderIndex().get());
        loadBalancer.get();
        Assertions.assertEquals(0, balanceStrategy.getRoundRobinProviderIndex().get());
        loadBalancer.get();
        Assertions.assertEquals(1, balanceStrategy.getRoundRobinProviderIndex().get());
    }

    @Test
    void roundRobin_whenProviderIsExcluded() throws ExecutionException, InterruptedException {
        LoadBalancer loadBalancer = new LoadBalancer();
        RoundRobinStrategy balanceStrategy = new RoundRobinStrategy();
        loadBalancer.setBalanceStrategy(balanceStrategy);

        loadBalancer.registerProviders(List.of(new SimpleProvider(), new SimpleProvider(), new SimpleProvider()));
        List<String> providerIds = loadBalancer.getProviders().stream().map(IProvider::getId).toList();

        loadBalancer.get();
        Assertions.assertEquals(0, balanceStrategy.getRoundRobinProviderIndex().get());

        loadBalancer.excludeProvider(providerIds.get(1));

        Assertions.assertEquals(providerIds.get(2), loadBalancer.get().get());
        Assertions.assertEquals(providerIds.get(0), loadBalancer.get().get());
        Assertions.assertEquals(providerIds.get(2), loadBalancer.get().get());
    }


    @Test
    void roundRobin_whenProviderIsIncludedBack() throws ExecutionException, InterruptedException {
        LoadBalancer loadBalancer = new LoadBalancer();
        RoundRobinStrategy balanceStrategy = new RoundRobinStrategy();
        loadBalancer.setBalanceStrategy(balanceStrategy);

        loadBalancer.registerProviders(List.of(new SimpleProvider(), new SimpleProvider(), new SimpleProvider()));
        List<String> providerIds = loadBalancer.getProviders().stream().map(IProvider::getId).toList();

        loadBalancer.get();
        Assertions.assertEquals(0, balanceStrategy.getRoundRobinProviderIndex().get());

        loadBalancer.excludeProvider(providerIds.get(1));
        Assertions.assertEquals(providerIds.get(2), loadBalancer.get().get());
        Assertions.assertEquals(1, balanceStrategy.getRoundRobinProviderIndex().get());

        loadBalancer.includeProvider(providerIds.get(1));
        // same provider will be returned again
        Assertions.assertEquals(providerIds.get(2), loadBalancer.get().get());
        Assertions.assertEquals(providerIds.get(0), loadBalancer.get().get());
        Assertions.assertEquals(providerIds.get(1), loadBalancer.get().get());
    }
}
